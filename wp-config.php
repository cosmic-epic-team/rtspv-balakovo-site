<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'balakovo' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'w,~-ucUv+8>,k|n1Ap}&^Opm_p*LM4w5`+vBT[z0W4>r<d433XEY2~ pp8OFPDvK' );
define( 'SECURE_AUTH_KEY',  'u{rwa-e#u6a_zexK_h}1:Tc(H0eL;q(u;]C%^e>]Qr&t*`0Q:R&2[pAqNp5A?<cm' );
define( 'LOGGED_IN_KEY',    'rix,Tv^Uj]<oomUwl([]p3I2IxDMscaED=5Y1! uZ^iv0b@ ^EYm=sa,w_8$2HBi' );
define( 'NONCE_KEY',        ')}gln-rK:[b^*.%_dBaW b(P,c1Y/B; :$8=KJ~O1odp>|auI)nYGYP=8Xn]fx,Y' );
define( 'AUTH_SALT',        '~/w`2r:D= 2.)AUr##9,dv+u@J{Zj[ !+-%ERY?^D-e{9PiD PJK23Td0s_RUqlQ' );
define( 'SECURE_AUTH_SALT', 'e]V]}U6K6uPA}AJGmD-`;W4De0`cX,~ugrZ0bIW`k)9N_Kx@DgDIG4e*ALlBjt<o' );
define( 'LOGGED_IN_SALT',   '$o=4JmmOG-B+G>xm}s$8zv0#huzQ`Uhe,aP2IdtL~qx^Bi_@p#F(RA1Qau/|72Dr' );
define( 'NONCE_SALT',       '#Y@@_yrkK8Z[yo(04t8UTjOBlk-&-ibu88%N+5v]D-Y7Ub2qGTIUKf<{%UP=*t`:' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
